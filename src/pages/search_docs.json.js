import { getSearchDocs } from "../../custom-astro-plugins/rehype/plaintext-frontmatter/index";

export const get = () => {
    return getSearchDocs(import.meta.glob(`./**/*.md*`));
}