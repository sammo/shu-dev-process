---
title: Delivery
description: This phase focuses on delivery of the product to the end user. It includes what you should be doing and submitting along with the product to ensure that the delivery runs as smoothly as possible.
---

import FilterContent from "@contentFilter";

## What is Expected?

<FilterContent options="4" block>
At level 4 you are expected to use manual delivery. Automated delivery and deployment to a service such as Azure is not expected at Level 4. You should complete the README below, it details the following:

1. Title of the project.
2. Names of the developers.
3. Release notes (what has been developed).
4. Contents of the submission (What files have been submitted).
5. How to compile/run the product.

</FilterContent>

<FilterContent options="5" block>
At level 5 the README file template builds on the sections in the Level 4 README as more information will be required for your delivery this year. some flavour of DevOps (such as CI) would be nice depending on the module. We would most like to see some DevOps culture integrated in your group assignments.

At Level 5, DevOps is not expected to be seen in all of your projects. In some modules where you have group assigments or work with clients however, it would be nice to see some basic DevOps such as a Continuous Integration pipeline. This can be done using a [Github Action](/shu-dev-process/en/planning/version-control/github-actions).
It is essential that you maintain good version control management in your group assigments or work with clients, and it would be nice to see a Github Action implemented for some basic automated testing. Manual communication should still be of high importance on this module, and during any of your other group projects.

Code should be pushed and merged in small batches frequently if you are following DevOps practices. If this is done in conjunction with automated tests in a CI workflow, you will impress your clients and your module leaders, and importantly, make for good quality code and releases. 

</FilterContent>

<FilterContent options="6" block>
At level 6 the README requires examples of uses, and a roadmap of your project amongst other things. DevOps culture should be adopted from Planning to Deployment. We are expecting CI/CD where appropriate. The DevOps culture would be most appropriate for your Final Year Projects and your group assignments. Deployment to a cloud service such as Azure or containerised deployment through Docker or Kubernetes would be nice to see.

At Level 6, we are expecting to see DevOps culture adopted for your larger projects, most notably your Final Year Project. It would also be nice to see in your group assignments. We are expecting each phase to be implemented, other than the final stage; **Monitor**, as you are not expected to maintain these projects after submission (although it may be beneficial to you to maintain your Final Year Projects). A CI/CD pipeline should be adopted at the minimum.

</FilterContent>

## Readme Templates

You should submit the README file for your level of study along with your other software artefacts.

<FilterContent options="4">
  [Level 4 README Template](/files/L4-README-Template.md)
</FilterContent>

<FilterContent options="5">
  [Level 5 README Template](/files/L5-README-Template.md)
</FilterContent>

<FilterContent options="6">
  [Level 6 README Template](/files/L6-README-Template.md)
</FilterContent>

<FilterContent options="5, 6" block>
## DevOps

At Level 5, some flavour of DevOps would be good to implement depending on the module. Group assignments would be ideal to implement some DevOps practices.

### What is DevOps

DevOps (Development Operations) is a set of practices that works to automate and integrate the processes between software development and IT teams, so they can build, test, and release software faster and more reliably (reference [1]).
You may not think this is very applicable to you as students, but it is a good idea to learn some form of DevOps before you start your first development job after university, as companies will more than likely have some form of IT team that developers like yourselves will need to work with.

DevOps is not a thing, such as a piece of software or programming language, it is a culture; a set of practices to help improve software development enabling for rapid software release. It is made up of 8 main practices, which can be seen from this diagram:

![DevOps diagram](../images/delivery/devops-diagram.png)
*Fig. 1: DevOps practices. Retrieved from reference [2]*

This diagram shows the primary DevOps practices, as well as some popular tools that are often used during each phase. Throughout each phase, teams collaborate and communicate to maintain alignment, velocity, and quality.
At this Level you should already be familiar with **Plan**, **Code** and **Build**. In this document, we will pay particular attention to **Release** and **Deployment**. These two phases are often called **Continuous Integration** and **Continuous Delivery/Deployment** (CI/CD). We will now go into detail about these two phases. The CI/CD pipeline is one of the best practices for DevOps teams to implement, for delivering code changes more frequently and reliably.

### Continuous Integration

Continuous Integration (CI) is the practice of automating the integration of code changes from multiple contributors into a single software project.
It allows developers to frequently merge code changes into a central repository where builds and tests then run. Automated tools are often used to assert the new code’s correctness before integration. At Level 5, using automated tools is not expected, but would be nice if you have the ability.

As you can probably guess, the backbone of a good CI pipeline/workflow is good version control management. Version control is also supplemented with other checks like automated code quality tests, and syntax style review tools. In Git (which you should be familiar with at this level), a CI pipeline can be implimented using an **Action**. I will demonstrate how to implement a Github Action later in the document.

CI is a valuable and well-established practice in modern, high performance software engineering organizations, therefore it is a good practice to learn, before you go out into the working world (reference [3]). You can set up a CI pipeline in Github using [Github Actions[(/shu-dev-process/en/planning/version-control/github-actions).

### Continuous Delivery and Deployment

Coninuous Delivery (CD) is slightly different, but goes hand in hand with Continuous Integration. Continuous Delivery is essentially an extension of Continuous Integration since it automatically deploys all code changes to a testing and/or production environment after the build stage.
This means that on top of automated testing, you have an automated release process where you can deploy your application any time.

In theory, with CD, you can decide to release daily, weekly, fortnightly, or whatever suits your business/user requirements. It is best practice to release as soon as possible, releasing in small, managable batches that are easy to troubleshoot if any problems arise.

Continuous Deployment goes one step further than Continuous Delivery. With this practice, every change that passes all stages of your production pipeline is released to your customers. There's no human intervention, and only a failed test will prevent a new change to be deployed to production (reference [4]). This is not expected at Level 5.

### Tools

There are many tools that can be used to set up a basic DevOps pipeline. We have already talked about Github Actions, [here](/shu-dev-process/en/planning/version-control/github-actions). Some other tools that are good for deployment can be seen below, they don't just have to be used for a DevOps pipeline:

- Codeberg - https://codeberg.org/
- Gitlab - https://about.gitlab.com/stages-devops-lifecycle/
- Gitea - https://gitea.io/en-us/
- Codeship - https://www.cloudbees.com/products/codeship>
- Jenkins - https://www.jenkins.io/

We recommend looking into the tools below for integrating the *full* DevOps pipeline into your projects.

### Testing

- JUnit: https://junit.org/junit5/
- NUnit: https://nunit.org/
- QUnit: https://qunitjs.com/
- Mocha: https://mochajs.org
- Jest:  https://jestjs.io
- Selenium: https://www.selenium.dev/

### Release

- GitHub Actions: https://github.com/features/actions
- Gitlab:https://about.gitlab.com/
- Gitea:https://gitea.io/en-us/
- Codeship:https://www.cloudbees.com/products/codeship
- Jenkins:https://www.jenkins.io/

### Deployment/Operate

- Docker:https://www.docker.com/
- Kubernetes:https://kubernetes.io/
- Azure:https://azure.microsoft.com/en-gb/ (free credit available from university)
- DC/OS:https://dcos.io/

</FilterContent>

<FilterContent options="6" block>

## Examples

### Continuous Integration on Github Actions

This action will install NodeJS and the dependencies your project requires, then build and test based on your `npm test` command. This will trigger only when you push to or make a pull request for your `main` branch. If any of your tests fail then so to will this action. You can even use [workflow badges](https://docs.github.com/en/actions/monitoring-and-troubleshooting-workflows/adding-a-workflow-status-badge) in the readme of your repository to quickly visualise the latest action result.

```yaml
name: Node.js CI
on:
  push:
    branches: [ main ]
  pull_request:
    branches: [ main ]
jobs:
  build:
    runs-on: ubuntu-latest 
    strategy:
      matrix:
        node-version: [16.14.x, 17.5.x]
    steps:
    - uses: actions/checkout@v2
    - name: Use Node.js ${{ matrix.node-version }}
      uses: actions/setup-node@v2
      with:
        node-versio n: ${{ matrix.node-version }}
        cache: 'npm'
    - run: npm ci
    - run: npm run build --if-present
    - run: npm test
```

* [View on GitHub Docs](https://docs.github.com/en/actions/automating-builds-and-tests/building-and-testing-nodejs-or-python)

### Continuous Deployment with GitHub Pages

GitHub allows you to host static webpages through their Pages platform.
### Webhooks

If your system wants to react to changes in another system it is possible to keep polling the system to ask "*has my data changed?*". This is computationally expensive and can use excessive amounts of bandwidth. Instead with webhooks, your system registers a callback URL with the external server that the external server then calls when the data has changed.
##### Polling (synchronous)

```mermaid
sequenceDiagram
    participant System
    participant External Server
    System->>External Server: Is my data ready?
    External Server->>System: No
    System->>External Server: Is my data ready?
    External Server->>System: No
    System->>External Server: Is my data ready?
    External Server->>System: No
    System->>External Server: Is my data ready?
    External Server->>System: Here's your data
```

##### Webhooks (asynchronous)

```mermaid
sequenceDiagram
    participant System
    participant External Server
    System->>External Server: Tell me when my data is ready.
    External Server->>System: Here's your data
```

For example, it's possible to receive Discord messages in a server based on events in a Git repository. You can read how to do this on [Discord's documentation](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks).

</FilterContent>

## References

<FilterContent options="5, 6" block>

[1] Atlassian. DevOps: Breaking the Development-Operations barrier. https://www.atlassian.com/devops.

[2] Medium. How to Become an DevOps Engineer in 2020. https://medium.com/swlh/how-to-become-an-devops-engineer-in-2020-80b8740d5a52. By Shane Shown.

[3] Atlassian. What is Continuous Integration?. https://www.atlassian.com/continuous-delivery/continuous-integration.

[4] Atlassian. Continuous integration vs. continuous delivery vs. continuous deployment. https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment.

</FilterContent>

[5] Markdown Guide: Basic Syntax - https://www.markdownguide.org/basic-syntax

