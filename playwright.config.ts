import { defineConfig, devices } from "@playwright/test";

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
// require('dotenv').config();

console.log("Setting up Playwright config...");
const IS_DEV_DEPLOY = process?.env?.NODE_ENV == "devdeploy";
const IS_CI = !!process.env?.CI;
const SITE = IS_DEV_DEPLOY
  ? "http://localhost:3000/shu-dev-process/"
  : "https://aserg.codeberg.page/shu-dev-process/";
/**
 * See https://playwright.dev/docs/test-configuration.
 */
console.log("Defining Playwright config...", IS_DEV_DEPLOY ? "(dev)" : "");
export default defineConfig({
  testDir: "./tests/specs",
  /* Maximum time one test can run for. */
  timeout: (IS_CI ? 30 : 120) * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000,
  },
  /* Run tests in files in parallel */
  fullyParallel: !IS_CI,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: IS_CI,
  /* Retry on CI only */
  retries: IS_CI ? 2 : 0,
  /* Opt out of parallel tests on CI. */
  workers: IS_CI ? 1 : undefined,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: "line",
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 0,
    /* Base URL to use in actions like `await page.goto('/')`. */
    baseURL: SITE,

    /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
    trace: "off",
  },
  
  projects: [
    /* Configure projects for different site versions */
    {
      name: "local",
      use: { 
        baseURL: "http://localhost:3000/shu-dev-process/",
        ...devices["Desktop Chrome"] },

    },
    {
      name: "live",
      use: { 
        baseURL: "https://aserg.codeberg.page/shu-dev-process/",
        ...devices["Desktop Chrome"] },

    },
    {
      name: "pages-develop",
      use: { 
        baseURL: "https://aserg.codeberg.page/shu-dev-process/@pages-develop",
        ...devices["Desktop Chrome"] },

    },
    /* Configure projects for major browsers */
    {
      name: "chromium",
      use: { ...devices["Desktop Chrome"] },
    },

    {
      name: "firefox",
      use: { ...devices["Desktop Firefox"] },
    },

    {
      name: "webkit",
      use: { ...devices["Desktop Safari"] },
    },

    /* Test against mobile viewports. */
    {
      name: "Mobile Chrome",
      use: { ...devices["Pixel 5"] },
    },
    {
      name: "Mobile Safari",
      use: { ...devices["iPhone 12"] },
    },

    /* Test against branded browsers. */
    {
      name: "Microsoft Edge",
      use: { channel: "msedge" },
    },
    {
      name: "Google Chrome",
      use: { channel: "chrome" },
    },
  ],

  /* Folder for test artifacts such as screenshots, videos, traces, etc. */
  // outputDir: 'test-results/',

  /* Run your local dev server before starting the tests */
  webServer: {
    command: `npm run preview`,
    port:3000,
    timeout: (IS_CI ? 120 : 300) * 1000,
    reuseExistingServer: true,
  },
});
