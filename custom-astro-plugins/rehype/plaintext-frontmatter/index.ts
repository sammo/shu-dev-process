import type { EndpointOutput } from "astro";
import Slugger from "github-slugger";

import { plaintextFrontmatter, mapGlobResult } from "./plaintext-frontmatter";
import {
  SearchDocument,
  GlobResult,
  PlaintextFrontmatterPluginOptions,
} from "./types.js";

type SearchIndexItems =
  | GlobResult
  | SearchDocument[]
  | Promise<SearchDocument[]>;

function isGlobResult(items: SearchIndexItems): items is GlobResult {
  return typeof items === "object" && !Array.isArray(items);
}

export async function getDocuments(
  items: SearchIndexItems,
  options?: PlaintextFrontmatterPluginOptions
): Promise<SearchDocument[]> {
  const documents = await (isGlobResult(items)
    ? mapGlobResult(items, options)
    : items);
  const sluggers = new Map<string, Slugger>();

  return documents
    .map((document) => {
      const { url, heading } = document;
      if (!url) return document;

      // add header slugs if missing
      if (heading && !url.includes("#")) {
        if (!sluggers.has(url)) sluggers.set(url, new Slugger());
        const headingSlug = sluggers.get(url)?.slug(heading);
        return {
          ...document,
          url: [url, headingSlug].join("#"),
        };
      }
      return document;
    })
    .map((doc) => {
      //this map keeps only front matter that is needed for searching
      const substituteTitleEnd = doc.url.indexOf("#") >= 0 ? doc.url.indexOf("#") : doc.url.length;
      const substituteTitle = doc.url.slice(0,substituteTitleEnd).replaceAll(new RegExp("[^A-z0-9]+","gmi")," ").trim();
      return {
        url: doc.url,
        title: doc?.title?.length ? doc.title : substituteTitle,
        heading: doc?.heading?.length ? doc.heading : doc.title,
        text: doc.text,
      };
    });
}

export async function getSearchDocs(
  items: SearchIndexItems,
  options?: PlaintextFrontmatterPluginOptions
): Promise<EndpointOutput> {
  const documents = await getDocuments(items, options);

  const sortFn = (a, b) => {
    return a.title > b.title ? 1 : a.title < b.title ? -1 : 0;
  };
  return { body: JSON.stringify(documents.sort(sortFn)) };
}

export default plaintextFrontmatter;
