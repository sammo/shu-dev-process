import { HastNode, HastElement, toText } from "hast-util-to-text";
import { toDom } from "hast-util-to-dom";
import { visit } from "unist-util-visit";

interface AstroVFile {
    data: { astro: { frontmatter: Record<string, any> } };
}

type RehypePlugin = (tree: HastNode, file: AstroVFile) => void;

/**
 * wraps 
 * @returns 
 */
export default function headingFragmentLinks(): RehypePlugin {
    return (tree: HastNode, { data }) => {
        let usedHeadings = new Set();
        visit(tree, 'element', function (node: HastElement) {
            const headings = ["h1", "h2", "h3", "h4", "h5", "h6"];
            const skip_heading = node.properties?.class && (node.properties.class + "").includes("no-anchor");
            if (headings.indexOf(node.tagName) != -1 && !skip_heading) {
                //for fragment links to work each heading must have an ID
                //so normalize new headings to just alphanumerical characters and replace whitespace with hyphens
                let newID = toText(node);
                if (newID) {
                    newID = newID.toLowerCase().replace(/[^A-z0-9\s]/gmi, "").replace(/\s+/gmi, "-");
                }
                let usingID = ("" + (node.properties?.id ?? newID)).toLowerCase();

                // ensure headings on a page are always unique by appending a number to them
                // if they're on the page more than once
                let unique_idx = 0;
                while (usedHeadings.has(usingID)) {
                    let tempID = `${usingID}${unique_idx}`;
                    if (!usedHeadings.has(tempID)) {
                        usingID = tempID;
                    }
                    ++unique_idx;
                }
                usedHeadings.add(usingID);
                node.properties.id = usingID;

                //this element wraps the children of the original heading in a new anchor that contains
                //the data needed to link to it, as well as adding a bold hashrtag that can be styled to 
                //appear on hover. It's this element that, users click to trigger the Web Share API.
                let hastAnchor: HastElement = {
                    type: 'element',
                    tagName: 'a',
                    properties: {
                        href: `#${usingID}`,
                        class: "heading-anchor"
                    },
                    children: [...node.children, {
                        type: 'element',
                        tagName: 'b',
                        properties: {
                            class: 'after heading-anchor-share',
                            title: 'Share'
                        },
                        children: []
                    }]
                }
                node.children = [hastAnchor];
            }
        });
    }
}